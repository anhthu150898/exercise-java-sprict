function GiaiThua(n)
{
    if (n<0)
        {
            return false;
        }
    else if (n==0 || n==1)
    {
        return 1;
    }
    else if (n>1)
    {
        return n*GiaiThua(n-1);

    }
   
}

let n=4;
if(GiaiThua(n)==false){
    console.log("du lieu nhap vao phai lon hon hoac bang 0");
}
else{
    console.log(GiaiThua(n));
}
