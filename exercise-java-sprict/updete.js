
    let personnel = [
        {
            ID: 3123,
            fullName: "Phan Van Teo",
            email: "teo_em@gmail.com",
            birthday: {day: 12, month: 3, year: 1980},
            age: 0,
            gender: "male",
            nativeLand: "Quang Nam Provice",
            position: "manager",
            salary: 0,
            seniority: 31
        },
        {
            ID: 3112,
            fullName: "Ban Thi No",
            email: "thi_no@gmail.com",
            birthday: {day: 4, month: 4, year: 1987},
            age: 0,
            gender: "female",
            nativeLand: "Ha Tinh Provice",
            position: "secterary",
            salary: 0,
            seniority: 20
        },
        {
            ID: 3376,
            fullName: "Ton That Hoc",
            email: "that.hoc@gmail.com",
            birthday: {day: 12, month: 3, year: 1998},
            age: 0,
            gender: "male",
            nativeLand: "Da Nang City",
            position: "employee",
            salary: 0,
            seniority: 9
        },
        {
            ID: 3377,
            fullName: "Ton That Nghiep",
            email: "that.nghiep@gmail.com",
            birthday: {day: 29, month: 2, year: 1996},
            age: 0,
            gender: "female",
            nativeLand: "Da Nang City",
            position: "employee",
            salary: 0,
            seniority: 50
        }
    ]
    function tinhtuoi() {
        let d = new Date();
        for (let i = 0; i < personnel.length; i++) {
            personnel[i].age = d.getFullYear() - personnel[i].birthday.year;
        }
    }
    tinhtuoi();
    for (let i = 0; i < personnel.length; i++) {
        console.log(personnel[i]);
    }